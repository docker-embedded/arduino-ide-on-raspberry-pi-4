FROM debian:latest AS base
ARG ARDUINO_VERSION=1.8.19
ARG TARGETPLATFORM

# Update and install necessary packages
RUN apt-get update && apt-get dist-upgrade -y \
    && apt-get install -y wget xz-utils arduino \
    && apt-get remove -y arduino \
    && apt-get clean

# Set the OS version to pull the debian packages from (using a "variable file")
RUN if [ $TARGETPLATFORM = "linux/arm64" ]; then echo "linuxaarch64" > arch; elif [ $TARGETPLATFORM = "linux/arm/v7" ]; then echo "linuxarm" > arch; else echo "linux64" > arch; fi;

# Download and extract the archive to /opt
RUN wget https://downloads.arduino.cc/arduino-$ARDUINO_VERSION-$(cat arch).tar.xz \
    && tar --verbose -xf arduino-$ARDUINO_VERSION-$(cat arch).tar.xz -C /opt \
    && rm arduino-$ARDUINO_VERSION-$(cat arch).tar.xz

# Change to the target directory and install the IDE
WORKDIR /opt/arduino-$ARDUINO_VERSION
RUN sh install.sh
RUN arduino --install-boards arduino:sam

# Use "--target base" switch when using the "docker build" command to skip the X11 addition
FROM base
RUN apt-get install -y firefox-esr libjssc-java \
    && apt-get clean
ENV HOME /home/root
CMD /usr/local/bin/arduino