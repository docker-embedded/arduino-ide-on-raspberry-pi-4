# Arduino IDE on Raspberry Pi 4
The full Arduino IDE running on a Raspberry Pi 4

## How to run GUI mode
To use X11 run the following command:
```
docker run --privileged -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY -h $HOSTNAME -v $HOME/.Xauthority:/home/root/.Xauthority registry.gitlab.com/docker-embedded/arduino-ide-on-raspberry-pi-4:x11
```

## Caveats
In order to use the serial devices on your Raspberry Pi you will need to add your existing user (outside of Docker) to various groups:
```
sudo usermod -aG tty $USER
sudo usermod -aG dialout $USER
```

You will also need to set your X11 display:
```
export DISPLAY=:0
```

On a Linux desktop machine you may run into a DISPLAY error such as [this one](https://github.com/gtrafimenkov/docker-codeswarm/issues/2). Just issue this command:
```
xhost +"local:docker@"
```